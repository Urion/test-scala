import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.{By, WebDriver}
import org.scalatest.{FlatSpec, _}
import org.scalatest.selenium.WebBrowser
import org.scalatest.time.{Seconds, Span}

import scala.language.postfixOps

class MyTestSpec extends FlatSpec with WebBrowser with Matchers with BeforeAndAfterAll {

  System setProperty("webdriver.chrome.driver", "./webdrivers/chromedriver_x64")
  implicit val webDriver: WebDriver = new ChromeDriver()
  implicitlyWait(Span(2, Seconds))

  val googleHomePage = "https://www.google.com/"
  val amazonHomePage = "https:/www.amazon.it/"

  /**
    * Open Google home page and check page title.
    */
  "Browser" should "open the correct page" in {
    // Open Google Chrome and go to Google home page
    go to googleHomePage

    // Check page title is Google
    pageTitle should be ("Google")
    }

  /**
    * Open amazon and search Scala for the Impatient using search bar.
    * Then check the price
    */
  "Browser" should "find on amazon 'Scala for the Impatient' book and check the price is 35,34 €" in {
    // Open Google Chrome and go to Amazon home page
    go to amazonHomePage

    // The element id of the amazon search bar
    val amazonSearchBar = "twotabsearchtextbox"

    // Search on Amazon the book 'scala for the impatient'
    textField(amazonSearchBar).value = "Scala for the impatient"
    submit()

    // Open product description
    webDriver findElement(By linkText "Scala for the Impatient") click

    // Element that contains the product price
    val priceElement = webDriver findElement(By xpath("//*[@id=\"buyNewSection\"]/div/div/span/span"))

    // Check the price is 35,34 €
    priceElement getText() should be ("EUR 35,34")

  }

  override protected def afterAll(): Unit = {
    // Close page after tests
    webDriver.close()
  }

}
