
// Sample build.sbt.
// Note:
//   Blank lines need to separate the statements.
//   := means you are setting the value for that key

name := "test-scala"

version := "0.1"

organization := "test"

scalaVersion := "2.12.7"

sbtVersion := "0.13"

libraryDependencies ++= Seq(
  "org.seleniumhq.selenium"   % "selenium-java"         % "2.35.0"  % "test",
  "org.scalatest"             % "scalatest_2.12"        % "3.0.5"   % "test"
)
  